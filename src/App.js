import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext';
// import { ProductProvider } from './ProductContext'
// import { Switch } from 'react-router'

import './App.css';
import AppNavBar from './components/AppNavBar';
import Products from './pages/Products';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';
import Logout from './pages/Logout';
import SpecificProduct from './pages/SpecificProduct'
import Cart from './pages/Cart';
import OrderHistory from './pages/OrderHistory';



function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  // const [productId, setProductId] = useState([])

  /*
  Since reloading the app resets our user state' properties to null, we need to re-retrieve the id and isAdmin values from our API

  To do so, we run a useEffect hook with a fetch request then re-set the user state's
  */

  useEffect(() => {
    // console.log(localStorage.getItem("token"))
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      // set our user state to include the user's id and isAdmin values
      // console.log(data.id)

      if(typeof data._id !== 'undefined' ){
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
      }else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
      // console.log(data.id)
    })
  }, [])
  return (
    <UserProvider value={{user, setUser}}>
       <div>
          <AppNavBar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/products" element={<Products />} />
              <Route path="/products/:productId" element={<SpecificProduct />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/orders" element={<OrderHistory />} />
              <Route path="*" element={<Error />} />
            </Routes>
          </Container>
       </div>
     </UserProvider> 
  );
}

export default App;
