import { useEffect, useState, useContext } from 'react';
import { Table, Button, Modal, Form, Stack } from 'react-bootstrap'
// import UserContext from '../UserContext'
import { useNavigate } from 'react-router-dom'

import OrderCard from '../components/OrderCard'


export default function OrderHistory() {

    const [ordersArr, setOrdersArr] = useState([]);
    // const [productsArr, setProductsArr] useState([])

    const token = localStorage.getItem("token")


    const fetchOrdersData = (e) => {
        // e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/orders/myOrders`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            },
        })
        .then(res => res.json())
        .then(data => {
            if(data) {
                setOrdersArr(data)
            }else {
                alert(`Something went wrong`)
            }
        })
    }

    // const fetchProductsData = (e) => {

    // }


    useEffect(() => {
        fetchOrdersData()
        // console.log(ordersArr)
    }, [])


    const orders = ordersArr.map(orders => {
        // console.log(orders)
        return(
            <OrderCard orderProp={orders} key={orders._id}/>
        )
    })

    

    return(
        <>
            <h1 className="text-center">ORDER HISTORY</h1>
            {orders}

        </>
    )
}