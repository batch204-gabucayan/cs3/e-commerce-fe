import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext'

export default function Register() {


	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	const{ user } = useContext(UserContext);
	const navigate = useNavigate();

	/*
	To properly change and save input values, we must implement two-way binding
	We need to capture whatever the user types in the input as they are typing
	Meaning we need the input's .value value
	To get the value, we capture the event (in this case, onChange). The target of the onChange event is the input, meaning we can get the .value
	*/

	useEffect(() => {
		// console.log(email)
		// console.log(password1)
		// console.log(password2)

		if((firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password1, password2]);

	function registerUser(e) {
		e.preventDefault() // prevent default form behavior, so that the form does not submit

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Duplicate email exists")
				setEmail("")
			}else{
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data){
						alert("User successfully registered")
						// redirect the user to the login page
						navigate("/login")
					}else {
						alert("Something went wrong")
					}
				})
			}
		})
		// alert('Thank you for registering!');
		// console.log(user.mail)
	}

	return(
	<>
		{(user.id === null) ?
			<Form onSubmit={e => registerUser(e)}>

				<Form.Group controld="userEmail">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter First Name"
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controld="userEmail">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter Last Name"
						value={lastName}
						onChange={e => setLastName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controld="userEmail">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control
						type="mobileNumber"
						placeholder="Enter your Mobile Number"
						value={mobileNo}
						onChange={e => setMobileNo(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controld="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter Email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controld="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter password"
						value={password1}
						onChange={e => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controld="password2">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{/*Conditional rendering will show things conditionally*/}

				{isActive ?
					<Button className="my-3" variant="primary" type="submit" id="submitBtn">
						Submit
					</Button> 
					:
					<Button className="my-3" variant="primary" id="submitBtn" disabled>
						Submit
					</Button> 
				}

			</Form>
			:
			<Navigate to="/"/>
		}
	</>
	)
}