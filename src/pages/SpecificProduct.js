import { useState, useEffect } from 'react';
import { Container, Card, Button, ButtonGroup } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';

export default function SpecificProduct() {


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1)
	// match.params holds the ID of our course in the courseId property
	// const courseId = match.params.courseId;
	const { productId } = useParams();

	const token = localStorage.getItem("token")
	const navigate = useNavigate();

	// console.log(productId)

	const addToCart = (e) => {
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/addToCart`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				alert(`Added to cart`)
				setQuantity(1)
				navigate("/products")
			}else {
				alert(`Something went wrong`)
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			// console.log(data)
			setName(data.productName)
			setDescription(data.description)
			setPrice(data.price)

		})
	}, [])

	const minusQuantity = () => {
		if(quantity > 1) {
			setQuantity(quantity - 1)
		}
	} 

	return(
		<Container className="mt-5">
			<Card>
				<Card.Body className="">
					{/*<Card.Subtitle>Name:</Card.Subtitle>*/}
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP {price}</Card.Text>
					<ButtonGroup vertical>
						<ButtonGroup className="mb-1">
							<Button variant="light outline-dark" size="sm" onClick={() => {setQuantity(quantity+1)}}>+</Button>
							<Button variant="light outline-dark">{quantity}</Button>
							<Button variant="light outline-dark" size="sm" onClick={minusQuantity}>-</Button>
						</ButtonGroup>
						<Button onClick={addToCart}>Add To Cart</Button>
					</ButtonGroup>
				</Card.Body>
			</Card>
		</Container>
	)
}
