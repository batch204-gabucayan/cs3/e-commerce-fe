import { useEffect, useState, useContext } from 'react';
import { Row, Col} from 'react-bootstrap'
// import courseData from '../data/courseData';
import ProductCard from '../components/ProductCard';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext'
// import ProductContext from '../ProductContext'

export default function Products() {

    // Check to see if the mock data was captured
    // console.log(courseData);
    // console.log(courseData[0])

    // Props
        // is a shorthand for "property" since components are considered as object in ReactJS.
        // Props is a way to pass data from a parent component to a child component.
        // It is synonymous to the function parameter.

    // Fetch on default always makes a GET request, unless a different one is specified
    // ALWAYS add fetch request for getting data in a useEffect hook
    const [productsData, setProductsData] = useState([])
    const { user } = useContext(UserContext);
    // const { setProductId } = useContext(ProductContext);

    

    const fetchData = () => {
        // console.log(`${process.env.REACT_APP_API_URL}`)
        fetch(`${process.env.REACT_APP_API_URL}/products/all`)
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            // setProductId(data)
            setProductsData(data);
            // console.log(productsData)
        })
    }

    console.log(user.isAdmin);
    useEffect(() => {
        fetchData();
    }, [])

    const products = productsData.map(products => {
        if(products.isActive){
            return (
                <ProductCard productProp={products} key={products._id}/>
            )
        }else{
            return null
        }
    })

    return(
        (user.isAdmin) ?
        <AdminView productsProp={productsData} fetchData={fetchData}/>
        :
        <Row lg={12} className="my-1 justify-content-center">
            <h1 className="text-center">Products</h1>
            {products}
        </Row>
    )
}