import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

// Added something

export default function Home(){

	const data = {
		title: "PC BASICS",
		content: "Your one stop shop for all your PC needs!",
		destination: "/courses",
		label: "Enroll now!"
	}
	return(
		<>
			<Banner dataProp={data} />
			<Highlights />
		</>
	)
}