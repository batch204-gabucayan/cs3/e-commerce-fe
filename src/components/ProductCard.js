import {Row, Col, Card, CardBody, Container} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import { Link, useNavigate } from 'react-router-dom';

// Destructuring is done in the parameter to retrieve the courseProp
export default function ProductCard({productProp}){

    // console.log(props.courseProp);
    // console.log(courseProp);

    const {_id, productName, description, price} = productProp;

    const navigate = useNavigate();

    // console.log(name);

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Syntax:
        // const [getter, setter] = useState(initalGetterValue)

    // When a component mounts (loads for the first time), any associated states will undergo a state change from null to the given initial/default state

    // e.g. count below goes from null to 0, since 0 is our initial state

    // const [count, setCount] = useState(0);
    // const [seats, setSeats] = useState(10);
    // const [seatCount, seatSetCount] = useState(30);

    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element.
    // console.log(useState(0));

    // Function that keeps track of the enrollees for a course
    // By default JavaScript is synchronous, as it executes code from top of the file all the to the bottom before it proceeds to the next
    // The setter function for useStates are asynchronous, allowing it to execute seperately from other codes in the program.
    // The "setCount" function is being executed with the "console.log" is already being completed resulting in the console to be behind by one count.

    // function enroll () {
    //     // setCount(count + 1);
    //     // console.log('Enrollees: ' + count);
    //     if(seats > 0) {
    //     setCount(count + 1);
    //     setSeats(seats - 1);
    //     } else {
    //         alert("no more seats available");
    //     }
    // }

    // useEffect makes any given code block happen when a state changes AND when a component first mounts (such as on initial page load)

    // in the example below, since count and seats are in the array of our useState, the code block will execute whenever those states change

    // if the array is blank, the code will be executed on component mount ONLY

    // DO NOT omit the array completely, for safety purposes.
    // useEffect(() => {
    //     if(seats > 0) {
    //         console.log('Enrollees: ' + count)
    //         console.log('Seats: ' + seats)
    //     } else {
    //         alert("no more seats available");
    //     }
    // }, [count, seats])

    // Syntax:
    /*
    useEffect(() => {
        code to be executed
    }, [state(s) to monitor])
    */
    const onClick = () => {
        // console.log(`${process.env.REACT_APP_API_URL}/products/${_id}`)
        {/*<Navigate to={`/products/${_id}`} />*/}
    }

    return(
        <Col xs={11} md={10} lg={3}>
            <Card className="mb-2 productCard" onClick={() => { navigate(`/products/${_id}`) }} style={{ cursor: "pointer" }}>
                <Card.Body>
                    <Card.Title>{productName}</Card.Title>
{/*                    <Card.Subtitle className="mb-0">Description:</Card.Subtitle>
                    <Card.Text className="mt-0">{description}</Card.Text>*/}
                    <Card.Subtitle className="mb-0">Price:</Card.Subtitle>
                    <Card.Text className="mt-0">PhP {price}</Card.Text>

                    {/*<Card.Text className="mt-0">Enrollees: {count}</Card.Text>
                    <Card.Text className="mt-0">Seats: {seats}</Card.Text>
                    <Button variant="primary" onClick={enroll}>Enroll!</Button>*/}

                    {/*<Link className="btn btn-primary me-1" to={`/products/${_id}`}>Details</Link>*/}
                    {/*<Link className="btn btn-secondary" to={`/products/${_id}`}>Order</Link>*/}
                </Card.Body>
            </Card>
        </Col>
    );
}

