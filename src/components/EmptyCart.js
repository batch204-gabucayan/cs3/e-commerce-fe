import {Row, Col, Card, CardBody, Button, Modal, Form} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
// import ProductContext from '../ProductContext'

// Destructuring is done in the parameter to retrieve the courseProp
export default function EmptyCard(){

    return(
    <>
    	<div className="m-5 p-5">
    		<h5 className="text-center">YOUR CART IS CURRENTLY EMPTY!</h5>
    	</div>
    </>

    );
}

