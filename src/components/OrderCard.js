import {Accordion, Row, Col, Container, Table} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import { Link, useNavigate } from 'react-router-dom';
import moment from 'moment'

// Destructuring is done in the parameter to retrieve the courseProp
export default function OrderCard(props){

    // console.log(props.orderProp);
    // console.log(courseProp);

    const {products, totalAmount, puchasedOn} = props.orderProp;

    // Comma separators function
    const commaSeparators = num => num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    const newAmount = commaSeparators(totalAmount)
    // const d = <Moment>puchasedOn</Moment>
    // console.log(puchasedOn)
    // console.log(products)

    const navigate = useNavigate();

    const onClick = () => {
        // console.log(`${process.env.REACT_APP_API_URL}/products/${_id}`)
        {/*<Navigate to={`/products/${_id}`} />*/}
    }

    const orderList = products.map(products => {
        return(
            <tr key={products._id}>
                <td>{products.productName}</td>
                <td>Php {commaSeparators(products.price)}</td>
                <td>{products.quantity}</td>
                <td>Php {commaSeparators((products.price)*(products.quantity))}</td>
            </tr>
        )
    })

    // useEffect(() => {
    //     const orderList = products.map(products => {
    //         return(
    //             <Row key={products._id}>
    //                 <Col>products.productName</Col>
    //                 <Col>products.price</Col>
    //                 <Col>products.quantity</Col>
    //                 <Col>products.subTotal</Col>
    //             </Row>
    //         )
    //     })
    // })

    return(
        <Accordion>
            <Accordion.Item eventKey="0">
              <Accordion.Header>
                <Container>
                  <Row lg={12}>
                    <Col className="m-auto">{moment(puchasedOn).format('MMMM D, YYYY')}</Col>
                    <Col className="m-auto">Total Amount: Php{newAmount}</Col>
                  </Row>
                </Container>
              </Accordion.Header>
              <Accordion.Body>
                <Table>
                    <thead className="bg-dark text-white">
                        <tr>
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orderList}
                    </tbody>
                </Table>
                
              </Accordion.Body>
            </Accordion.Item>
        </Accordion>
    );
}
