import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
    return(
        <Row className="my-3">
            <Col xs={12} md={4}>
                <Card.Title>
                    <h2 className="text-center">Motherboards</h2>
                </Card.Title>
                <img src="https://dlcdnimgs.asus.com/websites/global/products/sqlhk1j3w9jgpcci/img/z490/kv/hero.png" alt="asus motherboard" className="img-fluid"/>
            </Col>

            <Col xs={12} md={4}>
                <Card.Title>
                    <h2 className="text-center">Graphics Cards</h2>
                </Card.Title>
                <img src="https://static.bhphoto.com/images/images345x345/1618250747_1622297.jpg" alt="nvidia graphics card" className="img-fluid"/>
            </Col>

            <Col xs={12} md={4}>
                <Card.Title>
                    <h2 className="text-center">Monitors</h2>
                </Card.Title>
                <img src="https://redtech.lk/wp-content/uploads/2019/05/10869383_5.png" alt="asus monitor" className="img-fluid"/>
            </Col>
            
        </Row>
    )
}