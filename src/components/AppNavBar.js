// Long Method
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

// Short Method
import { useContext } from 'react';
import { Container, Nav, Navbar, NavDropdown} from 'react-bootstrap'
// import NavDropdown from 'react-bootstrap/NavDropdown';

// import { Link } from 'react-router-dom';
import { Link } from 'react-router-dom';

import UserContext from '../UserContext';


export default function AppNavBar() { 


	const { user } = useContext(UserContext);

		return(
			<Navbar bg="light" expand="lg">
			  <Container>
			    <Link className="navbar-brand" to="/">PC BASICS</Link>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			    <Navbar.Collapse id="basic-navbar-nav">
				{/*
					- className is use instead class, to specify a CSS class

					- changes for Bootstrap 5
					from mr -> to me
					from ml -> to ms
				*/}
			      <Nav className="ms-auto">
				      <Link className="nav-link" to="/">Home</Link>
				      <Link className="nav-link" to="/products">Products</Link>
				      <Link className="nav-link" to="/cart">Cart</Link>

			        {(user.id === null) ?
			        	<>

				        	<Link className="nav-link" to="/login">Login</Link>
				        	<Link className="nav-link" to="/register">Register</Link>
			        	</>
			        	:
			        	<>
			        		<Link className="nav-link" to="/orders">Order History</Link>
			        		<Link className="nav-link" to="/logout">Logout</Link>
			        	</>
			        }

			      </Nav>
			    </Navbar.Collapse>
			  </Container>
			</Navbar>
		)
	}
