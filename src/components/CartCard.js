import {Row, Col, Card, CardBody, Button, Modal, Form} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
// import ProductContext from '../ProductContext'

// Destructuring is done in the parameter to retrieve the courseProp
export default function CartCard(props){

    // console.log(props.courseProp);
    // console.log(courseProp);
    // console.log(fetchData)
    // console.log(cartsData)


    const [indexOfCart, setIndexOfCart] = useState(0)
    const [productsData, setProductsData] = useState("")
    const [showEdit, setShowEdit] = useState(false)

    const { cartProp, cartsData, fetchUserData} = props
    const {_id, productName, description, price, quantity} = cartProp;
    // const { productId } = useContext(ProductContext)
    const token = localStorage.getItem("token")

    const subTotal = (quantity*price)

    const closeEdit = () => {
        setShowEdit(false)
    }

    // const navigate = useNavigate();
    // function cartIndex(id){
    //     // e.preventDefault()
    //     for( let i = 0; i < cartsData.length; i++) {
    //         if((cartsData[i]._id) === id) {
    //             setIndexOfCart(i)
    //         }
    //     }
    // }

    const openEdit = (id) => {
        // console.log(productId)
        whatIndex(id)
        setShowEdit(true)
        // console.log(id)
        // fetchProductData(id)
    }

    const whatIndex = (id) => {
        for( let i = 0; i < cartsData.length; i++) {
            if((cartsData[i]._id) === id) {
                setIndexOfCart(i)
                setProductsData(cartsData[i].productId)
            }
        }
    }

    const addQuantity = (id) => {
        whatIndex(id)
        let add = quantity + 1
        editCart(add)
    }

    const subtractQuantity = (id) => {
        whatIndex(id)
        if(quantity > 1) {        
            let subtract = quantity - 1
            editCart(subtract)
        }else {
            openEdit(id)
        }
    }

    // fetch product
    // const fetchProductData = (dat) => {
    //     // console.log(`${process.env.REACT_APP_API_URL}`)
    //     console.log(dat)
    //     fetch(`${process.env.REACT_APP_API_URL}/products/all`)
    //     .then(res => res.json())
    //     .then(data => {
    //         setProductsData(data.productName);
    //         console.log(productsData)
    //     })
    // }

    // remove product
    const removeProduct = (e) => {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
                index: indexOfCart,
                id: productsData
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data) {
                alert("Product successfully removed")
                fetchUserData()
                closeEdit()
            }else {
                alert("Something went wrong")
            }
        })

    }

    const editCart = (num) => {
        // e.preventDefault()
        // whatIndex(id)
        fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
                index: indexOfCart,
                quantity: num
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data) {
                fetchUserData();
            }else {
                alert("Something went wrong")
            }
        })

    }
    
    // console.log(indexOfCart)



    return(
    <>
        <Card className="mb-2">
            <Card.Body >
                <Card.Title>{productName}</Card.Title>
                <Card.Subtitle className="mb-0">Description:</Card.Subtitle>
                <Card.Text className="mt-0">{description}</Card.Text>
                <Card.Subtitle className="mb-0">Price:</Card.Subtitle>
                <Card.Text className="mt-0">PhP {price}</Card.Text>
                <Card.Subtitle className="mb-0">Quantity:</Card.Subtitle>
                <Button variant="light" onClick={() => addQuantity(_id)}>+</Button>
                <Button variant="light">{quantity}</Button>
                <Button variant="light" onClick={() => subtractQuantity(_id)}>-</Button>
                <Button variant="danger" onClick={() => openEdit(_id)}>Remove</Button>
                <Card.Subtitle className="py-3"><Button variant="light">Subtotal: Php {subTotal}.00</Button></Card.Subtitle>
                {/*<Card.Text className="mt-0">PhP {price}</Card.Text>*/}

                {/*<Card.Text className="mt-0">Enrollees: {count}</Card.Text>
                <Card.Text className="mt-0">Seats: {seats}</Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll!</Button>*/}

                {/*<Link className="btn btn-primary me-1" to={`/products/${_id}`}>Details</Link>*/}
                {/*<Link className="btn btn-secondary" to={`/products/${_id}`}>Order</Link>*/}
            </Card.Body>
        </Card>

        {/*Edit Product Modal*/}
        <Modal show={showEdit} onHide={closeEdit}>
            <Form onSubmit={e => removeProduct(e)}> 
                <Modal.Header closeButton>
                    <Modal.Title>Are you sure you want to remove this?</Modal.Title>
                </Modal.Header>

                <Modal.Footer>
                    <Button variant="success" type="submit">Yes</Button>
                    <Button variant="secondary" onClick={closeEdit}>No</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    </>

    );
}

